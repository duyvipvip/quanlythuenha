var express      = require('express');
var app          = express();
var bodyParser   = require('body-parser');

var errorHandler = require('./middle-ware/error-handler');
var mongoose     = require('mongoose');
var RoomRouter = require('./routers/room.router');
var UserRouter = require('./routers/user.router');
var fileUpload = require('express-fileupload');
var AuthRouter = require('./routers/auth.router');
var HistoryRouter =require('./routers/history.router');
var ContactRouter =require('./routers/contact.router');
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');
    next();
};
app.use(allowCrossDomain);
app.use(express.static('public')); // de public cho client co the su dung duoc file trong thu muc do
app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



app.use('/api/Room',RoomRouter);
app.use('/api/User',UserRouter);
app.use('/api/Auth', AuthRouter);
app.use('/api/History',HistoryRouter);
app.use('/api/Contact',ContactRouter);
mongoose.connect('mongodb://localhost:27017/Broading_House',(err)=>{
// mongoose.connect('mongodb://havanduy:duy123@ds139370.mlab.com:39370/quanlythuenha',(err)=>{
    if(err){
        console.log('not connect to the database');
    } else {
        console.log('Successfully connected to MongoDB')
    }
})
app.use(errorHandler.errorHandler());
app.listen(8088,(err)=>{
        console.log('server run port 8088 http://localhost:8088');
})